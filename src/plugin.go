package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	hps "gitlab.com/gohabari/habari-plugin-system"
)

type column struct {
	name       string
	dataType   string
	isNullable bool
}

type primaryKey struct {
	name    string
	columns []column
}

type foreignKey struct {
	name      string
	columns   []column
	reference primaryKey
}

type table struct {
	name    string
	pk      primaryKey
	fks     []foreignKey
	columns []column
}

type view struct {
	name    string
	columns []column
}

type schema struct {
	name   string
	tables []table
	views  []view
}

type element struct {
	tableName       string
	metaDataUri     string
	restUri         string
	defaultPageSize int
}

type configuration struct {
	applicationName            string
	metaDataUri                string
	restUri                    string
	connectionType             string
	databaseEngine             string
	login                      string
	password                   string
	host                       string
	port                       int
	databaseName               string
	instanceName               string
	schemaName                 string
	maxSimultaneousConnections int
	defaultPageSize            int
	supportTransaction         bool
	allowSelect                bool
	allowInsert                bool
	allowUpdate                bool
	allowDelete                bool
	elements                   []element
}

type Db struct {
	cfg configuration
}

func (db Db) buildConnectionString() string {
	switch strings.ToLower(db.cfg.databaseEngine) {
	case "mariadb":
	case "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", db.cfg.login, db.cfg.password, db.cfg.host, db.cfg.port, db.cfg.databaseName)
	case "postgres":
		return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", db.cfg.host, db.cfg.port, db.cfg.login, db.cfg.password, db.cfg.databaseName)
		/*
			case "oracle":
				return fmt.Sprintf("oracle://%s:%s@%s:%d/%s", db.cfg.login, db.cfg.password, db.cfg.host, db.cfg.port, db.cfg.databaseName)
					case "sqlserver":
						return fmt.Sprintf("sqlserver://%s:%s@%s:%d/%s?app%%20name=%s", db.cfg.login, db.cfg.password, db.cfg.host, db.cfg.port, db.cfg.instanceName, db.cfg.applicationName)
		*/
	}

	return ""
}

func (db Db) getTablesSql() string {
	switch strings.ToLower(db.cfg.databaseEngine) {
	case "mariadb":
	case "mysql":
	case "postgres":
		return fmt.Sprintf("select t.table_name from information_schema.tables t where lower(t.table_schema) = lower('%s') order by 1", db.cfg.schemaName)
		/*
			case "oracle":
				return fmt.Sprintf("select o.object_name from as table_name all_objects o where o.object_type = 'TABLE' and lower(o.owner) = lower('%s') order by 1", db.cfg.schemaName)
			case "sqlserver":
				return fmt.Sprintf("select t.name as table_name from sys.tables t where lower(schema_name(t.schema_id)) = lower('%s') order by 1", db.cfg.schemaName)
		*/
	}

	return ""
}

func (db Db) getViewsSql() string {
	switch strings.ToLower(db.cfg.databaseEngine) {
	case "mariadb":
	case "mysql":
	case "postgres":
		return fmt.Sprintf("select v.table_name from information_schema.views v where lower(v.table_schema) = lower('%s') order by 1", db.cfg.schemaName)
		/*
			case "oracle":
				return fmt.Sprintf("select o.object_name from as view_name all_objects o where o.object_type = 'VIEW' and lower(o.owner) = lower('%s') order by 1", db.cfg.schemaName)
			case "sqlserver":
				return fmt.Sprintf("select v.name from sys.tables v where lower(schema_name(v.schema_id)) = lower('%s') order by 1", db.cfg.schemaName)
		*/
	}

	return ""
}

func (db Db) getTableColumnsSql(tableName string) string {
	switch strings.ToLower(db.cfg.databaseEngine) {
	case "mariadb":
	case "mysql":
	case "postgres":
		return fmt.Sprintf("select c.column_name, c.data_type, c.is_nullable from information_schema.columns c where lower(c.table_schema) = lower('%s') and lower(c.table_name) = lower('%s')", db.cfg.schemaName, tableName)
		/*
			case "oracle":
				return fmt.Sprintf("select ")
			case "sqlserver":
				return fmt.Sprintf("select ")
		*/
	}

	return ""
}

func (db Db) getViewColumnsSql(viewName string) string {
	switch strings.ToLower(db.cfg.databaseEngine) {
	case "mariadb":
	case "mysql":
	case "postgres":
		return fmt.Sprintf("select c.column_name, c.data_type, c.is_nullable from information_schema.columns c where lower(c.table_schema) = lower('%s') and lower(c.table_name) = lower('%s')", db.cfg.schemaName, viewName)
		/*
			case "oracle":
				return fmt.Sprintf("select ")
			case "sqlserver":
				return fmt.Sprintf("select ")
		*/
	}

	return ""
}

func (db Db) getTablePrimaryKeysSql(tableName string) string {
	switch strings.ToLower(db.cfg.databaseEngine) {
	case "mariadb":
	case "mysql":
	case "postgres":
		return fmt.Sprintf("select tc.constraint_name from information_schema.table_constraints tc where lower(tc.table_schema) = lower('%s') and lower(tc.table_name) = lower('%s') and lower(tc.constraint_type) = 'primary key'", db.cfg.schemaName, tableName)
		/*
			case "oracle":
				return fmt.Sprintf("select ")
			case "sqlserver":
				return fmt.Sprintf("select ")
		*/
	}

	return ""
}

func (db Db) initialize() error {

	return nil
}

func (db Db) Configure(bytes []byte) error {
	err := json.Unmarshal(bytes, &db.cfg)

	return err
}

func (db Db) Handle(s string, r *http.Request) (string, int, error) {
	return "", http.StatusOK, nil
}

func (db Db) Type() string {
	return "database"
}

func Plugin() hps.Plugin {
	return Db{}
}
